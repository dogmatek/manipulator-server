package ru.sber.jd.controllers;

import org.apache.tomcat.util.net.openssl.ciphers.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.sber.jd.dto.ProgramDto;

import java.util.List;

@RestController
public class SecurityController {

    @GetMapping("/")
    public String hello() {
        return "Добро пожаловать! Для запросов необходимо использовать следующие разделы /servo и /program ";
    }
}
