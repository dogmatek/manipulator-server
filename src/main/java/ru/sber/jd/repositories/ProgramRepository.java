package ru.sber.jd.repositories;

import org.springframework.data.repository.CrudRepository;
import ru.sber.jd.entities.ProgramEntity;

public interface ProgramRepository extends CrudRepository<ProgramEntity, Integer> {
}
