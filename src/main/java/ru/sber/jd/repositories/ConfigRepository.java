package ru.sber.jd.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import ru.sber.jd.entities.ConfigEntity;

import java.util.List;

public interface ConfigRepository extends CrudRepository<ConfigEntity, Integer> {

}
