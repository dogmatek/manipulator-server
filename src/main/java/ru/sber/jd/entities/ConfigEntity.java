package ru.sber.jd.entities;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDateTime;

// Конфигурация из базы данных
@Data
@Entity(name = "config")
public class ConfigEntity {

    @Id
    private String key;

    @Column
    private String value;

    @Column
    private String description;
}
