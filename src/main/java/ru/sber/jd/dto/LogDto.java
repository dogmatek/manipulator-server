package ru.sber.jd.dto;

import javax.persistence.Column;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class LogDto {
    private Integer id;
    private String key;
    private String description;
    private LocalDateTime dt;
}
